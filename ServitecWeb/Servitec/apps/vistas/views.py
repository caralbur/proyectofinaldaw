from django.shortcuts import render,render_to_response

# Create your views here.
def index(request):
	return render(request,'servitec/index.html')
def nosotros(request):
	return render(request,'servitec/nosotros.html')
def trabajos(request):
	return render(request,'servitec/trabajos.html')
def consejos(request):
	return render(request,'servitec/consejosLadrillos.html')
def creadores(request):
	return render(request,'servitec/creadores.html')
def contactos(request):
	return render(request,'servitec/contactos.html')
