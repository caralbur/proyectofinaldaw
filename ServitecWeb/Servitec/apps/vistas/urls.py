from django.conf.urls import url, include
from apps.vistas import views
from apps.frontend import views as frontviews
app_name="vistas"
urlpatterns = [
    url(r'^$',views.index,name='index'),
    url(r'^nosotros/$',views.nosotros,name='nosotros'),
    url(r'^trabajos/$',views.trabajos,name='trabajos'),
    url(r'^consejosLadrillos/$',views.consejos,name='consejosLadrillos'),
    url(r'^creadores/$',views.creadores,name='creadores'),
    url(r'^contactos/$',views.contactos,name='contactos'),
    url(r'^api/trabajo/$', frontviews.trabajo_list),
    url(r'^api/trabajo/(?P<pk>[0-9]+)$', frontviews.trabajos_detail),
   
]