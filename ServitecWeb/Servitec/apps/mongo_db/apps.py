from django.apps import AppConfig


class MongoDbConfig(AppConfig):
    name = 'mongo_db'
