from django.contrib import admin
from .multiDBmodel import MultiDBModelAdmin
# Register your models here.
from .models import Trabajo, Factura, CostoTrabajo, Articulo, Insumo, Cliente, Telefono, ClienteNatural, ClienteJuridico, TrabajoEvidencia
admin.site.register(Trabajo)
admin.site.register(Factura)
admin.site.register(CostoTrabajo)
admin.site.register(Articulo)
admin.site.register(Insumo)
admin.site.register(Cliente)
admin.site.register(Telefono)
admin.site.register(ClienteNatural)
admin.site.register(ClienteJuridico)
admin.site.register(TrabajoEvidencia,MultiDBModelAdmin)
