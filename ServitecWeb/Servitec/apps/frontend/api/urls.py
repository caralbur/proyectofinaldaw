from django.urls import path
from .views import TrabajoViewSet, FacturaViewSet, ArticuloViewSet, ClienteViewSet, CostoTrabajoViewSet, ArticuloViewSet, InsumoViewSet, TelefonoViewSet, ClienteJViewSet, ClienteNViewSet,EvidenciaViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'trabajo',TrabajoViewSet,base_name='trabajo')
router.register(r'factura', FacturaViewSet, base_name="factura")
router.register(r'articulo', ArticuloViewSet, base_name="articulo")
router.register(r'cliente', ClienteViewSet, base_name="cliente")
router.register(r'evidencia', EvidenciaViewSet, base_name="evidencia")
urlpatterns = router.urls
