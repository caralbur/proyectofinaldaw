from rest_framework import viewsets
from rest_framework.generics import ListAPIView, RetrieveUpdateDestroyAPIView
from apps.frontend.models import Trabajo,  Factura, CostoTrabajo, Articulo, Insumo, Cliente, Telefono, ClienteNatural, ClienteJuridico,TrabajoEvidencia
from .serializers import TrabajoSerializer, FacturaSerializer, CostoTrabajoSerializer, ArticulosSerializer, InsumosSerializer, ClienteSerializer, TelefonoSerializer, ClienteNaturalSerializer, ClienteJuridicoSerializer,TrabajoEvidenciaSerial

class TrabajoViewSet(viewsets.ModelViewSet):
    queryset = Trabajo.objects.all()
    serializer_class = TrabajoSerializer

class FacturaViewSet(viewsets.ModelViewSet):
    queryset = Factura.objects.all()
    serializer_class = FacturaSerializer

class CostoTrabajoViewSet(viewsets.ModelViewSet):
    queryset = CostoTrabajo.objects.all()
    serializer_class = CostoTrabajoSerializer

class ArticuloViewSet(viewsets.ModelViewSet):
    queryset = Articulo.objects.all()
    serializer_class = ArticulosSerializer

class InsumoViewSet(viewsets.ModelViewSet):
    queryset = Insumo.objects.all()
    serializer_class = InsumosSerializer

class TelefonoViewSet(viewsets.ModelViewSet):
    queryset = Telefono.objects.all()
    serializer_class = TelefonoSerializer

class ClienteViewSet(viewsets.ModelViewSet):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer

class ClienteNViewSet(viewsets.ModelViewSet):
    queryset = ClienteNatural.objects.all()
    serializer_class = ClienteNaturalSerializer


class ClienteJViewSet(viewsets.ModelViewSet):
    queryset = ClienteJuridico.objects.all()
    serializer_class = ClienteJuridicoSerializer

class EvidenciaViewSet(viewsets.ModelViewSet):
    queryset = TrabajoEvidencia.objects.all()
    serializer_class = TrabajoEvidenciaSerial
