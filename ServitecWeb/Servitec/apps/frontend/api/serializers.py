from rest_framework import serializers
from apps.frontend.models import Trabajo,  Factura, CostoTrabajo, Articulo, Insumo, Cliente, Telefono, ClienteNatural, ClienteJuridico,TrabajoEvidencia

class TrabajoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trabajo 
        fields = ('id','descripcion','valorUnitario', 'estaTerminado')

class FacturaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Factura
        fields = ('id','idCliente','fecha','subtotal','total')

class CostoTrabajoSerializer(serializers.ModelSerializer):
    class Meta:
        model = CostoTrabajo
        fields = ('id','cantidad','idTrabajo','idFactura')

class ArticulosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Articulo
        fields = ('id','nombre','precio')

class InsumosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Insumo
        fields = ('id','idFactura','idArticulo')

class ClienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cliente 
        fields = ('id','nombre','direccion')

class TelefonoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Telefono 
        fields = ('id','idCliente','telefono')

class ClienteNaturalSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClienteNatural 
        fields = ('id','idCliente','apellido')

class ClienteJuridicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClienteJuridico
        fields = ('id','idCliente','razonSocial')


class TrabajoEvidenciaSerial(serializers.ModelSerializer):
    class Meta:
        model = TrabajoEvidencia
        fields = ('id','name', 'idTrabajo', 'image')



