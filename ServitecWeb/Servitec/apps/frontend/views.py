from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Trabajo 
from .api.serializers import *

# Create your views here.

@api_view(['GET', 'POST'])
def trabajo_list(request):
    """
 enlista los trabajos , o crea un nuevo trabajo
 """
    if request.method == 'GET':
        data = []
        nextPage = 1
        previousPage = 1
        trabajos = Trabajo.objects.all()
        page = request.GET.get('page', 1)
        paginator = Paginator(trabajos, 10)
        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)

        serializer = TrabajoSerializer(data,context={'request': request} ,many=True)
        if data.has_next():
            nextPage = data.next_page_number()
        if data.has_previous():
            previousPage = data.previous_page_number()

        return Response({'data': serializer.data , 'count': paginator.count, 'numpages' : paginator.num_pages, 'nextlink': '/api/trabajos/?page=' + str(nextPage), 'prevlink': '/api/trabajos/?page=' + str(previousPage)})

    elif request.method == 'POST':
        serializer = TrabajoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def trabajos_detail(request, pk):
    """
    Elimina, actualiza or toma un trabajo por la id.
 """
    try:
        trabajo = Trabajo.objects.get(id=pk)
    except trabajo.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = TrabajoSerializer(trabajo,context={'request': request})
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = TrabajoSerializer(trabajo, data=request.data,context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        customer.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)





