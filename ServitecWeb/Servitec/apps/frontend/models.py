from django.db import models
from django.db import connection
from djongo import models as modelsdjongo
import django.db.models.options as options
options.DEFAULT_NAMES = options.DEFAULT_NAMES + ('in_db',)
# Create your models here.

class Trabajo(models.Model):
	descripcion = models.CharField(max_length=50)
	valorUnitario = models.FloatField()
	estaTerminado = models.BooleanField(default=True)
	
	def __str__(self):
		return self.descripcion

class Factura(models.Model):
	idCliente = models.IntegerField()
	fecha = models.DateField()
	subtotal= models.FloatField()
	total= models.FloatField()

class CostoTrabajo(models.Model):
	cantidad = models.IntegerField()
	idTrabajo = models.ForeignKey(Trabajo,null=True,blank=True,on_delete=models.CASCADE)
	idFactura = models.ForeignKey(Factura,null=True,blank=True,on_delete=models.CASCADE)

class Articulo(models.Model):
	nombre = models.CharField(max_length=20)
	precio = models.FloatField()

class Insumo(models.Model):
	idFactura = models.ForeignKey(Factura,null=True,blank=True,on_delete=models.CASCADE)
	idArticulo = models.ForeignKey(Articulo,null=True,blank=True,on_delete=models.CASCADE)

class Cliente(models.Model):
	nombre = models.CharField(max_length=20)
	direccion = models.CharField(max_length=30)

class Telefono(models.Model):
	idCliente = models.ForeignKey(Cliente,null=True,blank=True,on_delete=models.CASCADE)
	telefono = models.CharField(max_length=10)

class ClienteNatural(models.Model):
	idCliente = models.ForeignKey(Cliente,null=True,blank=True,on_delete=models.CASCADE)
	apellido = models.CharField(max_length=20)


class ClienteJuridico(models.Model):
	idCliente = models.ForeignKey(Cliente,null=True,blank=True,on_delete=models.CASCADE)
	razonSocial = models.TextField()

class TrabajoEvidencia(models.Model):
	name = models.CharField(max_length=255)
	idTrabajo = models.ForeignKey(Trabajo,null=True,blank=True,on_delete=models.CASCADE)
	image = models.ImageField(null=True, max_length=255)
	class Meta:
		in_db = "monguito" 