from django.conf.urls import url 
from apps.usersDivision.views import RegistroUsuario,ListarUsuario 

 
urlpatterns = [ 
	url(r'^registrar', RegistroUsuario.as_view(), name="usuario_registrar"), 
	url(r'^listar', ListarUsuario.as_view(), name="usuario_listar"),
]
