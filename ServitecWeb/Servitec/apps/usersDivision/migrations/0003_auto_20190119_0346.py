# Generated by Django 2.1.5 on 2019-01-19 03:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('usersDivision', '0002_auto_20190119_0342'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='user',
            options={'verbose_name': 'user', 'verbose_name_plural': 'users'},
        ),
        migrations.AlterModelTable(
            name='user',
            table=None,
        ),
    ]
