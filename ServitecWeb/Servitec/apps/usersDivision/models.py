from django.db import models
from django.contrib.auth.models import AbstractUser
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill
# Create your models here.

class User(AbstractUser):
    is_admin = models.BooleanField(default=False)
    is_client= models.BooleanField(default=False)

class Client(models.Model):
    bio = models.TextField(max_length=200, blank= True)
    ruc = models.CharField(max_length=50, blank=True)
    birthday = models.DateField(null=False, blank=False, auto_now=False, auto_now_add=False)
    #part to add an imageField names like 'avatar' have to be changed this is made using django-imagekit
    avatar = models.ImageField(upload_to='avatars')
    avatar_thumbnail = ImageSpecField(source='avatar',
                                      processors=[ResizeToFill(100, 50)],
                                      format='JPEG',
                                      options={'quality': 60})

