from rest_framework.generics import ListAPIView, RetrieveUpdateDestroyAPIView

from apps.usersDivision.models import User, Client

from apps.usersDivision.api.serializers import UserSerializer, ClientSerializer

class UserListView(ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserUpDeRe(RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class ClienteListView(ListAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

class ClienteUpDeRe(RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

