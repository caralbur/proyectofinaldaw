from django.urls import path
from .views import UserListView, UserUpDeRe, ClienteListView, ClienteUpDeRe

urlpatterns = [
    path('user/',UserListView.as_view()),
    path('user/<pk>',UserUpDeRe.as_view()),
    path('cliente/', ClienteListView.as_view()),
    path('cliente/<pk>', ClienteUpDeRe.as_view())
]