from rest_framework import serializers
from apps.usersDivision.models import User, Client

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User 
        fields = ('id','last_login','is_superuser',
        'first_name','last_name','email','is_staff',
        'is_active','is_admin','is_client',"username")
    
class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client 
        fields = ('id','bio', 'ruc', 'birthday')