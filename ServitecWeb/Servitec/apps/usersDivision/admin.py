from django.contrib import admin

# Register your models here.

from .models import User, Client

admin.site.register(User)
admin.site.register(Client)
