from django.contrib.auth.models import User 
from django.contrib.auth.forms import UserCreationForm 
from django.views.generic import CreateView,ListView 
from django.urls import reverse_lazy 

from apps.usersDivision.forms import RegistroForm 

from django.contrib.auth import get_user_model
User = get_user_model()

class ListarUsuario(ListView): 
	model=User 
	template_name = 'listar.html' 

 

class RegistroUsuario(CreateView):
	model = User 
	form_class = RegistroForm 
	template_name = "registrar.html" 
	success_url = reverse_lazy('usuario_listar') 

