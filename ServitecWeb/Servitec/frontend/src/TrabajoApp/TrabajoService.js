import axios from 'axios';
const API_URL = 'http://127.0.0.1:8000';

export default class TrabajoService{

    constructor(){}

    getTrabajos() {
        const url = `${API_URL}/api/trabajo/`;
        return axios.get(url).then(response => response.data);
    }  
    getTrabajosByURL(link){
        const url = `${API_URL}${link}`;
        return axios.get(url).then(response => response.data);
    }
    getTrabajos(pk) {
        const url = `${API_URL}/api/trabajo/${pk}`;
        return axios.get(url).then(response => response.data);
    }
    deleteTrabajos(work){
        const url = `${API_URL}/api/trabajo/${work}`;
        return axios.delete(url);
    }
    createTrabajo(work){
        const url = `${API_URL}/api/trabajo/`;
        return axios.post(url,work);
    }
    updateTrabajo(work){
        const url = `${API_URL}/api/trabajo/${work.id}/`;
        return axios.put(url,work);
    }
}
