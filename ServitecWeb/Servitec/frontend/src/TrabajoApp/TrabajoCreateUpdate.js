import React, { Component } from 'react';
import TrabajoService from './TrabajoService';
import { Checkbox } from 'antd';
import axios from 'axios';

const trabajoService = new TrabajoService();
let esTerminado = false;
class TrabajoCreateUpdate extends Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.state  = {
          terminado: false,
          formLayout: 'horizontal',
      };
      }
    
      handleFormLayoutChange = (e) => {
        this.setState({ formLayout: e.target.value });
      }
      
      
      onChange(e) {
        esTerminado = e.target.checked; 
      }
      
      componentDidMount(){
        const { match: { params } } = this.props;
        if(params && params.id)
        {
          trabajoService.getTrabajos(params.id).then((c)=>{
            this.refs.descripcion.value = c.descripcion;
            this.refs.valorUnitario.value = c.valorUnitario;
            this.refs.estaTerminado.value = c.estaTerminado;
          })
        }
      }

      handleCreate(){
        trabajoService.createTrabajo(
          {
            descripcion:this.refs.descripcion.value,
            valorUnitario:this.refs.valorUnitario.value,
            estaTerminado:esTerminado,
        }          
        ).then((result)=>{
          alert("Trabajo creado!");
        }).catch(()=>{
          alert('Error Please re-check your form');
        });
      }
      handleUpdate(pk){
        const { match: { params } } = this.props;
        const ref = this.refs.descripcion.value;
        const uni = this.refs.valorUnitario.value;
        axios.put(`http://127.0.0.1:8000/api/trabajo/${params.id}/`,
          {
            descripcion:ref,
            valorUnitario:uni,
            estaTerminado:esTerminado
        })
        .then((result)=>{
          console.log(result);
          alert("Trabajo actualizado!");
        })
      }

      handleSubmit(event) {
        const { match: { params } } = this.props;

        if(params && params.id){
          this.handleUpdate(params.id);
        }
        else
        {
          this.handleCreate();
        }

        event.preventDefault();
      }

      render() {
        const { formLayout } = this.state;
        const formItemLayout = formLayout === 'horizontal' ? {
          labelCol: { span: 4 },
          wrapperCol: { span: 14 },
        } : null;
        const buttonItemLayout = formLayout === 'horizontal' ? {
          wrapperCol: { span: 14, offset: 4 },
        } : null;
        return (
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label>
                Descripcion:</label>
                <input className="form-control" type="text" ref='descripcion'  />
  
              <label>
                valor unitario:</label>
                <input className="form-control" type="number" ref='valorUnitario'/>
  
              <label>
                Estado: </label>
                <Checkbox onChange={this.onChange}>Terminado</Checkbox>
              <div className="col-12 col-xl-12 col-md-12">
              <input className="btn btn-primary " type="submit" value="Submit" />
              </div>
              </div>
          </form>      

        );
      }  
}

export default TrabajoCreateUpdate;