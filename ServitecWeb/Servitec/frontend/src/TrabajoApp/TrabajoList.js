import  React, { Component } from  'react';
import  TrabajoService  from  './TrabajoService';
import axios from 'axios';

const  trabajoService  =  new  TrabajoService();

class  TrabajoList  extends  Component {

constructor(props) {
    super(props);
    this.state  = {
        customers: [],
    };
}

componentDidMount() {
    var  self  =  this;
    axios.get('http://127.0.0.1:8000/api/trabajo/')
    .then(function (result) {
        self.setState({ customers:  result.data})
    });
}
handleDelete(e,pk){
    var  self  =  this;
    trabajoService.deleteTrabajos(pk).then(()=>{
        var  newArr  =  self.state.customers.filter(function(obj) {
            return  obj.pk  !==  pk;
        });
        self.setState({customers:  newArr})
    });
    this.props.history.push('/trabajo-list/');
    this.forceUpdate();
}

render() {

    return (
        <div  className="trabajos--list">
            <table  className="table">
            <thead  key="thead">
            <tr>
                <th>#</th>
                <th>Descripcion</th>
                <th>valor unitario</th>
                <th>Estado terminado</th>
            </tr>
            </thead>
            <tbody>
            {this.state.customers.map( c  =>
                <tr  key={c.id}>
                <td>{c.id}</td>
                <td>{c.descripcion}  </td>
                <td>{c.valorUnitario}</td>
                <td>{c.estaTerminado ? "Terminado":"Pendiente"}</td>
                <td>
                <button className="btn btn-primary m-1"  onClick={(e)=>  this.handleDelete(e,c.id) }> Delete</button>
                <a className="btn btn-primary"   href={"/trabajo/" + c.id}>Actualizar</a>
                </td>
            </tr>)}
            </tbody>
            </table>
            <button  className="btn btn-primary"  onClick=  {  this.nextPage  }>Next</button>
            <button  className="btn btn-primary m-1" onClick= {(e)=>  window.location.href = "/trabajo/" }>Nuevo</button>
        </div>
        );
  }
}
export  default  TrabajoList;