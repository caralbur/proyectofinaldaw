import  React, { Component } from  'react';
import { BrowserRouter } from  'react-router-dom'
import { Route, Link } from  'react-router-dom'
import  TrabajoList  from  './TrabajoApp/TrabajoList'
import  TrabajoCreateUpdate  from  './TrabajoApp/TrabajoCreateUpdate'
import {connect} from 'react-redux';
import * as actions from './store/actions/auth';
import  './App.css';

import LayoutServitec from './containers/Layout';
import Presentation from './components/Presentation';
import Consejos from './components/Consejos';
import Estadisticas from './components/Estadisticas';
import Contactenos from './components/Contactenos';
import Nosotros from './components/Nosotros';
import Creadores from './components/Creadores';
import Trabajos from './components/Trabajos';
import CotizacionList from './CotizacionApp/CotizacionList';
import CotizacionUpdate from './CotizacionApp/CotizacionUpdate';
import ConsejoPintura from './components/ConsejoPintura';
import ConsejoConstruccion from './components/ConsejoConstruccion';
import ConsejoDiseno from './components/ConsejoDiseno';
import Login from './containers/Login';
import SignUp from './containers/SignUp';


import Profile from './components/profile';
import Dashboard from './components/Dashboard';

const BaseLayout = () => (

    <div className="content">
      <Route path="/" component ={Presentation} exact/>
      <Route path="/consejos" component ={Consejos}/>
      <Route path="/consejopintura" component ={ConsejoPintura}/>
      <Route path="/consejoconstruccion" component ={ConsejoConstruccion}/>
      <Route path="/consejodiseno" component ={ConsejoDiseno}/>
      <Route path="/estadisticas" component ={Estadisticas}/>
      <Route path="/contactenos" component ={Contactenos}/>
      <Route path="/nosotros" component ={Nosotros}/>
      <Route path="/creadores" component ={Creadores}/>
      <Route path="/trabajos" component ={Trabajos}/>
      <Route path="/trabajo-list/" exact component={TrabajoList} />
      <Route path="/trabajo/:id"  component={TrabajoCreateUpdate} />
      <Route path="/trabajo/" exact component={TrabajoCreateUpdate} />
      <Route path="/cotizacion/" exact component={CotizacionList} />
      <Route path="/cotizacion/:id" exact component={CotizacionUpdate} />
      <Route path="/login/" component={Login} />
      <Route path="/signup/" component={SignUp} />
      <Route path="/profile" component={Profile} />
      <Route path="/dashboard/" component={Dashboard} />
    </div>
)

class App extends Component {
  
  componentDidMount(){
    this.props.onTryAutoSignup();
  }

  render() {
    return (
      
      <div className="App">
        <LayoutServitec {...this.props}>
          <BrowserRouter>
            <BaseLayout/>
          </BrowserRouter>
        </LayoutServitec>
      </div>
    );
  }
}

const mapStatetoProps = state => {
  return{
    isAuthenticated: state.token !== null,
    isAdmin : localStorage.getItem("adminToken")!== null
    //isAdmin : state.tokenAdmin !== null
  }
}

const mapDispatchToProps = dispatch=>{
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState())
  }
}

export default connect(mapStatetoProps,mapDispatchToProps)(App);
