import * as actionTypes from './actionTypes'
import axios from 'axios';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    }
}

export const authSuccess = token =>{
    return {
        type : actionTypes.AUTH_SUCCESS,
        token: token
    }
}

export const authFail = error =>{
    return{
        type: actionTypes.AUTH_FAIL,
        error: error
    }
}

export const authAdmin = () =>{
    return{
        type: actionTypes.AUTH_ADMIN
    }
}

export const logout = () =>{
    localStorage.removeItem("user");
    localStorage.removeItem('expirationDate');
    localStorage.removeItem("userna");
    const tokenAdmin = localStorage.getItem("adminToken");
    if(tokenAdmin!==undefined){
        localStorage.removeItem('adminToken');
    } 
    return{
        type: actionTypes.AUTH_LOGOUT
    }
}

const checkAuthTimeout = expirationTime => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, expirationTime*1000)
    }
}

export const authLogin = (username, password)=>{
    return dispatch =>{
        dispatch(authStart());
        axios.post("http://127.0.0.1:8000/rest-auth/login/",{
            username: username,
            password: password
        })
        .then(res =>{
            const token = res.data.key;
            const expirationDate = new Date(new Date().getTime()+3600*1000*24);
            localStorage.setItem('token',token);
            localStorage.setItem('expirationDate',expirationDate);
            dispatch(authSuccess(token));
            dispatch(checkAuthTimeout(3600*24));
            console.log(token);
        })
        .catch(err =>{
            dispatch(authFail(err))
        })
    }
}

export const authSignup = (username, email,password1,password2)=>{
    return dispatch =>{
        dispatch(authStart());
        axios.post("http://127.0.0.1:8000/rest-auth/registration",{
            username: username,
            password1 : password1,
            password2: password2
        })
        .then(res =>{
            const token = res.data.key;
            const expirationDate = new Date(new Date.getTime()+3600*1000*24);
            localStorage.setItem('token',token);
            localStorage.setItem('expirationDate',expirationDate);
            dispatch(authSuccess(token));
            dispatch(checkAuthTimeout(3600*24));
        })
        .catch(err =>{
            dispatch(authFail(err))
        })
    }
}

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem("token");
        const tokenAdmin = localStorage.getItem("adminToken");
        if(tokenAdmin==undefined || tokenAdmin==null){
            console.log("auth",tokenAdmin);
            dispatch(authAdmin());
        }
        if(token == undefined){
            dispatch(logout());
        }else{
            const expirationDate = new Date(localStorage.getItem("expirationDate"));
            if(expirationDate <= new Date()){
                dispatch(logout());
            }else{
                dispatch(authSuccess(token));
                dispatch(checkAuthTimeout( (expirationDate.getTime()- new Date().getTime() )/1000))
            }
        }
    }
}

export const authCheckAdmin = (userName)=>{
    return dispatch=>{
    axios.get('http://127.0.0.1:8000/api-autent/user/')
        .then(res =>{
        {res.data.map((e, i) => 
            {if (e.username==userName && e.is_superuser===true){
                localStorage.setItem('adminToken',e.username);
                dispatch(authAdmin());     
                }
            })
        }
    })
    }
}

