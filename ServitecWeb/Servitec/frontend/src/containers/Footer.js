import React, { Component } from 'react';
import 'antd/dist/antd.css';
import MapServitec from './MapServitec';
import logo from "../img/logo.png";
import MapWithAMarker from './MapServitec';

const FooterServitec = (props)=>{
    return (
    <div id="footer">
        <div className="footer-wrap">
            <div className="ant-col-xs-24 ant-col-sm-24 ant-col-md-6">
                <img src={logo} alt="Error en imagen"/>
            </div>
            <div className="ant-col-xs-24 ant-col-sm-24 ant-col-md-6">
                <div className="footer-center">
                    <ul id="bottomList" className="d-none d-md-block">
                        <li className="col-md-12 col-12 list-inline-item"><h5 className="col-md-12 col-12">Puntos importantes</h5></li>
                        <li className="col-md-12 col-12 list-inline-item"><a id="footerI" href="/">INICIO</a></li>
                        <li className="col-md-12 col-12 list-inline-item"><a id="footerN" href="/nosotros/">NOSOTROS</a></li>
                        <li className="col-md-12 col-12 list-inline-item"><a id="footerT" href="/trabajos/">TRABAJOS</a></li>
                        <li className="col-md-12 col-12 list-inline-item"><a id="footerC" href="/consejos/">CONSEJOS</a></li>
                        <li className="col-md-12 col-12 list-inline-item"><a id="footerCn" href="/contactenos/">CONTACTO</a></li>
                    </ul>
                </div>
            </div>
            <div className="ant-col-xs-24 ant-col-sm-24 ant-col-md-6">
            <h5 className="col-md-12 col-12">Dónde nos encontramos</h5>
                <p className="col-md-12 col-12"> Dir: Mucho Lote 4ta etapa MZ 2519 V 26 </p>
                <p className="col-md-12 col-12"> Tel: 0994123771 </p>
            </div>
            <div className="ant-col-xs-24 ant-col-sm-24 ant-col-md-6">
                <h5 className="col-md-12 col-12">Ubicación en el mapa</h5>
                <MapWithAMarker>
                containerElement={<div className="ant-col-xs-24 ant-col-sm-24 ant-col-md-6" 
                style={{ height: '25px' }}/>}
                mapElement={<div className="ant-col-xs-24 ant-col-sm-24 ant-col-md-6" 
                style={{ height: '25px' }}/>}
                </MapWithAMarker>
            </div>
        </div>

    </div>
    );
}

export default FooterServitec;