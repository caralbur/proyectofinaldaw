import React from 'react';
import {Form, Icon, Input, Button,Spin} from 'antd';
import { NavLink } from 'react-router-dom';
import {connect} from 'react-redux';
import * as actions from '../store/actions/auth';
import axios from 'axios';
const FormItem = Form.Item;
const antIcon = <Icon type = "loading" style = {{fontSize:24}} spin/>;

class NormalLoginForm extends React.Component {
    
    state = {
        data: [],
    }

    handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
            this.props.onAuth(values.userName, values.password)     
            this.props.onAdmin(values.userName)  
            localStorage.setItem('userna',values.userName);
            this.props.history.push('/');
        }
        else{
            alert("Su contraseña o usuario están mal escritos");
        }
      });

    }
  
    render() {
      const { getFieldDecorator } = this.props.form;
      let errorMessage = null;
      if(this.props.error){
          errorMessage = (
              <p>({this.props.error.message})</p>
          )
      }
      return (
        <div>
            {errorMessage}
        {
            this.props.loading ?
            <Spin indicator={antIcon}/>
            :

            
            <Form onSubmit={this.handleSubmit} className="login-form">
                <Form.Item>
                    {getFieldDecorator('userName', {
                    rules: [{ required: true, message: 'Ingrese su Usuario' }],
                    })(
                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('password', {
                    rules: [{ required: true, message: 'Ingrese su contraseña' }],
                    })(
                    <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                    )}
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" className="m-2 login-form-button">
                    Log in
                    </Button>
                    Or 
                    <NavLink className = "m-2" to = "/signup/">
                    Registrarse ahora!
                    </NavLink>
                </Form.Item>
            </Form>

        }
                    </div>
      );
    }
  }

const WrappedNormalLoginForm = Form.create()(NormalLoginForm);

const mapStateToProps = (state) => {
    return {
        loading: state.loading,
        error:state.error,
    }
}

const mapDispatchToProps = dispatch =>{
    return{
        onAuth: (username, password)=> dispatch(actions.authLogin(username,password)),
        onAdmin : (username) => dispatch(actions.authCheckAdmin(username)),
    }
}

const Login = connect(mapStateToProps, mapDispatchToProps)(WrappedNormalLoginForm);
export default Login;