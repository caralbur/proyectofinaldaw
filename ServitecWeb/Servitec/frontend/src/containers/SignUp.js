import React, { Component } from 'react';
import {Form, Input, Icon, Select, Row, Col, Checkbox, Button} from 'antd';
import {connect} from 'react-redux';
import * as actions from '../store/actions/auth';
import { NavLink } from 'react-router-dom';

const { Option } = Select;

  class RegistrationForm extends React.Component {
    state = {
      confirmDirty: false,
    };
  
    handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) => {
        if (!err) {
            this.props.onAuth(values.userName,
            values.email, 
            values.password,
            values.confirm) 
            this.props.history.push('/');         
        }else{
          alert("La contraseña ingresada es muy simple");
        }
      });

    }
  
    handleConfirmBlur = (e) => {
      const value = e.target.value;
      this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    }
  
    compareToFirstPassword = (rule, value, callback) => {
      const form = this.props.form;
      if (value && value !== form.getFieldValue('password')) {
        callback('Las dos contraseñas ingresadas no son iguales');
      } if(value.length<8){
        callback('Las dos contraseñas ingresadas son muy simples requieren una segura');
      }else {
        callback();
      }
    }
  
    validateToNextPassword = (rule, value, callback) => {
      const form = this.props.form;
      if (value && this.state.confirmDirty) {
        form.validateFields(['confirm'], { force: true });
      }
      callback();
    }
  
    render() {
      const { getFieldDecorator } = this.props.form;
  
      return (
        <div>
        <Form onSubmit={this.handleSubmit}>
          <Form.Item>
            {getFieldDecorator('userName', {
            rules: [{ required: true, message: 'Ingrese su Usuario' }],
            })(
            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
            )}
          </Form.Item>
          <Form.Item  label="E-mail" >
            {getFieldDecorator('email', {
              rules: [{
                type: 'email', message: 'No es un correo válido!',
              }, {
                required: true, message: 'Ingrese su E-mail!',
              }],
            })(
              <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email"/>
            )}
          </Form.Item>
          <Form.Item  label="Password">
            {getFieldDecorator('password', {
              rules: [{
                required: true, message: 'Ingrese su contraseña',
              }, {
                validator: this.validateToNextPassword,
              }],
            })(
              <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
            )}
          </Form.Item>
          <Form.Item  label="Confirm Password">
            {getFieldDecorator('confirm', {
              rules: [{
                required: true, message: 'Confirme su contraseña',
              }, {
                validator: this.compareToFirstPassword,
              }],
            })(
              <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" onBlur={this.handleConfirmBlur} />
            )}
          </Form.Item>
          <Form.Item >
            {getFieldDecorator('agreement', {
              valuePropName: 'checked',
            })(
              <Checkbox>Acepto los términos y condiciones <a href="">Acepto</a></Checkbox>
            )}
          </Form.Item>
          <Form.Item >
            <Button type="primary" htmlType="submit">Registrarse</Button>
          </Form.Item>
          Or
          <NavLink className = "m-2" to = "/login/">
             Login
            </NavLink>
        </Form>
        </div>
      );
    }
  }
  
  const mapStateToProps = (state) => {
    return {
        loading: state.loading,
        error:state.error
    }
}

const mapDispatchToProps = dispatch =>{
    return{
        onAuth: (username, email, password1, password2)=> dispatch(actions.authSignup(username,email,password1, password2))
    }
}


  const WrappedRegistrationForm = Form.create()(RegistrationForm);

  const SignUp = connect(mapStateToProps, mapDispatchToProps)(WrappedRegistrationForm)
  export default SignUp;
  