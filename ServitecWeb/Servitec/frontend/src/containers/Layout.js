import React from 'react';
import 'antd/dist/antd.css';
import FooterServitec from './Footer';
import logo from "../img/logo.png";
import * as actions from '../store/actions/auth';
import {connect} from 'react-redux';
import {Layout, Menu, Breadcrumb, Icon,} from 'antd';

import { Anchor } from 'antd';
import MenuItem from 'antd/lib/menu/MenuItem';
const { Link } = Anchor;

  const { SubMenu } = Menu;
  const {
    Header, Content, Footer, Sider,} = Layout;

class LayoutServitecDB extends React.Component {
   render (){
    return (
        <Layout className="layout">
            <Header className="sticky-top header">
            <div className="logo" />
            <Menu
                theme="dark"
                mode="horizontal"
            >
                <Menu.Item key="0"><img className="img-fluid w-25" src={logo} alt="Servitec"/></Menu.Item>
                <Menu.Item key="1"><a href={"/"}>Inicio</a></Menu.Item>
                <Menu.Item key="2"><a href={"/nosotros/"}>Nosotros</a></Menu.Item>
                <Menu.Item key="3"><a href={"/contactenos/"}>Contáctenos</a></Menu.Item>
                <Menu.Item key="4"><a href={"/consejos/"} >Consejos</a></Menu.Item>
                <Menu.Item key="5"><a href={"/creadores/"}>Creadores</a></Menu.Item>
                <Menu.Item key="6"><a href={"/trabajos/"}>Trabajos</a></Menu.Item>
                {this.props.isAuthenticated ?
                    <Menu.Item key="7"><a href={"/estadisticas/"}>Estadisticas</a></Menu.Item>
                    :
                    <></>
                }
                {this.props.isAuthenticated ?
                    <Menu.Item key="8"><a href={"/cotizacion/"}>Cotizacion</a></Menu.Item>
                    :
                    <></>
                }
                {this.props.isAuthenticated  && this.props.isAdmin ?
                    <Menu.Item key="9"><a href={"/trabajo-list/"}>Proforma</a></Menu.Item>
                    :
                    <></>                
                }
                {this.props.isAuthenticated  && this.props.isAdmin ?
                    <Menu.Item key="9"><a href={"/dashboard/"}>Tablero</a></Menu.Item>
                    :
                    <></>                
                }
                {this.props.isAuthenticated && !this.props.isAdmin ? 
                    <Menu.Item Key="10"><a href={"/profile/"}>Perfil</a></Menu.Item>
                    :
                    <></>
                }
                {this.props.isAuthenticated  && this.props.isAdmin ?
                    <Menu.Item key="10"><a target="_blank" href={"http://127.0.0.1:8000/admin/"}>Administracion</a></Menu.Item>
                    :
                    <></>                
                }
                {this.props.isAuthenticated?
                <Menu.Item key="11"><a onClick={this.props.logout} href={"/"}>Logout</a></Menu.Item>
                :
                <Menu.Item key="7"><a href={"/login/"}>Login</a></Menu.Item>
                }
                
            </Menu>
            </Header>
            <Content >
            <Breadcrumb >
                <Breadcrumb.Item>Inicio</Breadcrumb.Item>
                <Breadcrumb.Item>Trabajos</Breadcrumb.Item>
                <Breadcrumb.Item>Login</Breadcrumb.Item>
            </Breadcrumb>
            <Layout className="row" >

                <Content className="col-md-12 col-sm-12 col-12 border-bottom border-secondary" >
                {this.props.children}
                </Content>
            </Layout>
            </Content>
            <Footer style={{ textAlign: 'center' }}>
                <FooterServitec>

                </FooterServitec>
            </Footer>
        </Layout>
        );
    }
};


const mapDispatchToProps = dispatch =>{
    return{
        logout: ()=> dispatch(actions.logout())
    }
}


  const LayoutServitec = connect(null,mapDispatchToProps)(LayoutServitecDB);
  export default LayoutServitec;