import React, { Component } from 'react';
import axios from 'axios';
import CotizacionList from '../CotizacionApp/CotizacionList';
import {
    Form, Input, Button, Radio,InputNumber,
  } from 'antd';
  
class CustomForm extends React.Component {
    constructor() {
      super();
      this.state = {
        formLayout: 'horizontal',
      };
    }
  
    handleFormLayoutChange = (e) => {
      this.setState({ formLayout: e.target.value });
    }
    
    handleFormSubmit = (event, requestType, articleID)=>{
        event.preventDefault();
        const nom = event.target.elements.nombre.value;
        const pre = event.target.elements.precio.value;
        console.log(nom,pre);

        switch(requestType){
          case "post":
            return axios.post('http://127.0.0.1:8000/api/articulo/',{
              nombre:nom,
              precio:pre
            })
            .then(window.location.reload())
            .catch(error => console.err(error));
            
          case "put":
            return axios.put(`http://127.0.0.1:8000/api/articulo/${ articleID }/`,{
              nombre:nom,
              precio:pre
            })
            .then(window.location.reload())
            .catch(error => console.err(error));
        }
    }

    render() {
      const { formLayout } = this.state;
      const formItemLayout = formLayout === 'horizontal' ? {
        labelCol: { span: 4 },
        wrapperCol: { span: 14 },
      } : null;
      const buttonItemLayout = formLayout === 'horizontal' ? {
        wrapperCol: { span: 14, offset: 4 },
      } : null;
      return (
        <div>
          <Form layout={formLayout} onSubmit={(event) => this.handleFormSubmit(
            event,
            this.props.requestType,
            this.props.articleID )}>
            <Form.Item label="Layout" {...formItemLayout}>
              <Radio.Group defaultValue="horizontal" onChange={this.handleFormLayoutChange}>
                <Radio.Button value="horizontal">Horizontal</Radio.Button>
                <Radio.Button value="vertical">Vertical</Radio.Button>
                <Radio.Button value="inline">Inline</Radio.Button>
              </Radio.Group>
            </Form.Item>
            <Form.Item label="Nombre" {...formItemLayout}>
              <Input name="nombre" placeholder="Nombre del insumo" />
            </Form.Item>
            <Form.Item label="Descripcion" {...formItemLayout} >
              <Input name="descrip" placeholder="Descripcion del insumo" />
            </Form.Item>
            <Form.Item  {...formItemLayout} label="Precio" >
             <InputNumber name="precio" min={0} />
            <span className="ant-form-text"> Precio</span>
            </Form.Item>
            <Form.Item {...buttonItemLayout}>
              <Button type="primary" htmlType="submit">{this.props.btnText}</Button>
            </Form.Item>
          </Form>
        </div>
      );
    }
  }
  

export default CustomForm;