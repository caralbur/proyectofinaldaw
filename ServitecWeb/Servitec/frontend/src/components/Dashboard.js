import React from 'react';
import {Table, Input, Button, Statistic, Card, Row, Col, Icon } from 'antd';
import axios from 'axios';
const Countdown = Statistic.Countdown;
const deadline = Date.now() + 1000 * 60 * 60 * 24 * 2 + 1000 * 30; // Moment is also OK

class Dashboard extends React.Component {
    state = {
        trabajos: [],
        productos: [],
        usuarios: [],
        num_trabajos : 0,
        num_cotizaciones : 0,
        num_trabajos_terminados : 0,
        imagenes: [],
    }

    componentDidMount() {
        axios.get('http://127.0.0.1:8000/api/articulo/')
        .then(res =>{
            this.setState({
                productos: res.data,
                num_cotizaciones : res.data.length,
            });
        })
        axios.get('http://127.0.0.1:8000/api/trabajo/')
        .then(res =>{
            this.setState({
                trabajos: res.data,
                num_trabajos : res.data.length,
            });
            {res.data.map((e, i) => 
                {if (e.estaTerminado==true){
                    this.setState({
                        num_trabajos_terminados:+1
                    });
                }
                })
            }
        })
        axios.get('http://127.0.0.1:8000/api-autent/user/')
        .then(res =>{
            this.setState({
                usuarios: res.data,
            });
        })
        axios.get('http://127.0.0.1:8000/api/evidencia/')
        .then(res=>{
            this.setState({
                imagenes: res.data,
            })
        })
    }


    render(){
        var columns = [{
            title: 'ID',
            dataIndex: 'id',
            width: 150,
          }, {
            title: 'Descripcion',
            dataIndex: 'descripcion',
            width: 150,
          }, {
            title: 'ID trabajo',
            dataIndex: 'fecha',
          },{
            title: 'Imagen',
            dataIndex: 'imagen',
          }];
        
        var data = [];
        {this.state.imagenes.map((e, i)=>{
            data.push({
              id: e.id,
              descripcion: e.name,
              fecha: e.idTrabajo ,
              imagen: <a target="_blank" href={e.image} >Evidencia</a>,
            });
        })}
        return(
            <div className="row align-center">
                <div className="col-6 col-sm-12 col-md-12 col-xl-4 m-2 ">
                <h2 className="col-md-12">Tiempo real en Uso</h2>
                    <Row className="col-md-12" gutter={16}>
                        <Col span={24} >
                        <Countdown title="Tiempo en Uso" value={deadline} />
                        </Col>

                        <Col span={24} className="m-2">
                        <Countdown title="Día" value={deadline} format="D día H hora" />
                        </Col>
                    </Row>
                </div>
              
              <div className="col-6 col-sm-12 col-md-12 col-xl-4 p-3">
                <Row classNAme="col-12" gutter={16}>
                <h2 classNAme="col-12">Resultados con Trabajos</h2>
                <Col span={12}>
                    <Card className="col-md-12">
                    <Statistic
                        title="Trabajos Terminados"
                        value={this.state.num_trabajos_terminados/this.state.num_trabajos*100}
                        precision={2}
                        valueStyle={{ color: '#3f8600' }}
                        prefix={<Icon type="arrow-up" />}
                        suffix="%"
                    />
                    </Card>
                </Col>
                <Col span={12}>
                    <Card className="col-md-12">
                    <Statistic
                        title="Trabajos Pendientes"
                        value={(this.state.num_trabajos-this.state.num_trabajos_terminados)/this.state.num_trabajos*100}
                        precision={2}
                        valueStyle={{ color: '#cf1322' }}
                        prefix={<Icon type="arrow-down" />}
                        suffix="%"
                    />
                    </Card>
                </Col>
                </Row>
            </div>
            <div className="col-12 col-md-12 col-xl-12 p-3">
            <Row gutter={16}>
                <h2>Datos Generales</h2>
                <Col span={8}>
                    <Card>
                        <Statistic
                            title="Elementos Cotizados"
                            value={this.state.num_cotizaciones}
                            precision={2}
                            valueStyle={{ color: '#111347' }}
                            prefix={<Icon type="arrow-up" />}
                            suffix=""
                        />
                    </Card>
                </Col>
                <Col span={8}>
                    <Card>
                        <Statistic
                            title="Trabajos Realizados"
                            value={this.state.num_trabajos}
                            precision={2}
                            valueStyle={{ color: '#111347' }}
                            prefix={<Icon type="arrow-up" />}
                            suffix=""
                        />
                    </Card>
                </Col>
                <Col span={8}>
                    <Card>
                        <Statistic
                            title="Usuarios Registrados"
                            value={this.state.usuarios.length}
                            precision={2}
                            valueStyle={{ color: '#111347' }}
                            prefix={<Icon type="arrow-up" />}
                            suffix=""
                        />
                    </Card>
                </Col>
            </Row>
            </div>
            <div className="col-2 col-md-2 p-3"></div>
            <div className="col-8 col-md-8 col-xl-8 p-3">
            <h2>Evidencia de Trabajos Realizados</h2>
            <Table columns={columns} dataSource={data} pagination={{ pageSize: 10 }} scroll={{ y: 240 }} />,
            </div>
            <div className="col-2 col-md-2 p-3"></div>
            </div>
        )
    }

}

export default Dashboard;





  