import React, { Component }  from 'react';
import 'antd/dist/antd.css';
import {
    select,
    scaleBand,
    scaleLinear,
}from 'd3';
import { updateLocale } from 'moment';

class BarChart extends React.Component {
    constructor(){
        super();
        this.state = {
            data: []
        }
    }
    
    componentWillMount() {
        fetch("https://api.tradingeconomics.com/historical/country/Ecuador/indicator/GDP%20From%20Construction?client=guest:guest")
        .then((response) => {
            return response.json()
          })
          .then((art) => {
            this.setState({ data: art })
          })    
    }



    componentDidMount() {
        this.draw();
    }

    draw(){
        const node=select(this.node);
        const bounds= node.node().getBoundingClientRect();
        const w=bounds.width;
        const h= bounds.height;
        const { data }= this.state;

        
        const xscale=scaleBand();    
        xscale.domain(this.state.data.map(d=>d.Value));
        xscale.range([0,w]);
        xscale.padding(0.1);

        const yscale=scaleLinear();
        yscale.domain([0,100]);
        yscale.range([0,h]);

        const upd=node.selectAll('rect').data(this.state.data);
        upd.enter()
            .append('rect')
            .attr('x',d=>xscale(d.Value))
            .attr('y',d=>h-yscale(d.Value))
            .attr('width',xscale.bandwidth())
            .attr('height',d=> yscale(d.Value))
            .attr('fill','skyblue');
    }

    componentDidUpdate(){
        this.draw();
    }

    render(){
        return(
            <div className="row">
                <div className="col-md-6 col-6">
                    <table border="2">
                        <thead>
                            <tr>
                                <th>Country</th>
                                <th>HistoricalDataSymbol</th>   
                                <th>DateTime</th>
                                <th>GDP</th>
                            </tr>
                        </thead>
                        <tbody>  
                            {this.state.data.map(art => {
                            return (
                                <tr>
                                    <td>{art.Country}</td>
                                    <td>{art.HistoricalDataSymbol}</td>
                                    <td>{art.DateTime}</td>
                                    <td>{art.Value}</td>
                                </tr>
                            );
                            })}
                        </tbody>
                    </table>
                </div>

                <div className="col-md-6 col-6">
                    <div className="col-md-12 col-12">
                            <h3> Grafica GDP vs Tiempo </h3>
                    </div>
                    <div className="col-md-12 col-12">
                        <svg
                        style={{width:'80%', height:'70%'}}
                        ref={node=>{
                            this.node=node;
                        }}                    
                        >
                        </svg>
                    </div>
                    
                </div>
            </div> 
            
        );
    }

}

export default BarChart;
