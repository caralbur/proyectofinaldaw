import React, { Component } from 'react';
import 'antd/dist/antd.css';
import pinturalatex from '../img/latex.jpg';
import pinturasintetica from '../img/sint.jpg';
import pinturabase from '../img/base.jpg';
import pinturaacustica from '../img/acustica.jpg';
import { Layout } from 'antd';
import { Carousel } from 'antd';

class ConsejoPintura extends Component{
    render(){
        return(
        <Layout>
            <div id='contenidoCons'>
                    <div className="row">
                        <header className="col-xl-12">
                        <h1 id="hdConsejos" className="col-xl-12">Consejos</h1>
                        </header>
                    </div>

                    <div className="row">
                        <section id="secConsejos" className="col-md-12 col-12">
                        <h3 id="tl0" className="col-xl-12">.</h3>
                        <div className="row">
                            <article className="col-md-12">
                            <h3 > Tipos de pintura y usos</h3>
                            <h4 > Pintura plástica o látex  </h4>
                            <img className="imagenCons col-md-12 col-12" style={{ height: '60%', width: '30%' }} src={pinturalatex} alt="Imagen not found"/>
                            <p className="descCons"> La pintura plástica o látex es la más común en el uso de interiores. Esta pintura es inodora y seca rápido, aparte de poder ser lavada fácilmente con agua. La pintura plástica puede ser usada en casi cualquier superficie menos madera, ya que su base de agua puede dañarla. </p>
                            </article>
                        </div>

                        <div>
                            <article>
                            <h4 > Pintura sintética </h4>
                            <img className="imagenCons col-md-12 col-12" style={{ height: '60%', width: '30%' }} src={pinturasintetica} alt="Imagen not found"/>
                            <p className="descCons"> La pintura sintética es más durable, pero también seca más lento y se necesitara disolventes para limpiarla. a pintura sintética es generalmente usada en bordes y marcos, por su durabilidad. Esta pintura no debe ser usada sin pintura base, pero puede ser usada en madera. </p>
                            </article>
                        </div>
                        <div>
                            <article>
                            <h4> Pintura base </h4>
                            <img className="imagenCons col-md-12 col-12" style={{ height: '60%', width: '30%' }} src={pinturabase} alt="Imagen not found"/>
                            <p className="descCons"> La pintura base es usada para preparar una superficie antes de pintar. Esta pintura suaviza superficies, y hará que las otras capas se vean mejor. </p>
                            </article>
                        </div>
                        <div>
                            <article>
                            <h4> Pintura acústica </h4>
                            <img className="imagenCons col-md-12 col-12" style={{ height: '60%', width: '30%' }} src={pinturaacustica} alt="Imagen not found"/>
                            <p className="descCons"> Este tipo de pintura es usada para prevenir ruidos. La pintura acústica o anti-ruido es usada en baldosas o azulejos acústicos, y ayudara a mantener las cualidades de estas. </p>
                            </article>
                        </div>
                        </section>
                    </div>

                    
                    <Carousel autoplay>
                        <div><h3><a href={"/consejoconstruccion/"}>¿Como construir tu propia casa?</a></h3></div>
                        <div><h3><a href={"/consejos/"}>Conoce los tipos de ladrillos y sus principales usos</a></h3></div>
                        <div><h3><a href={"/consejodiseno/"}>¿Como se si mi casa esta bien diseñada?</a></h3></div>
                    </Carousel>
                       

            
            </div>
        </Layout>
        )
    };

}

export default ConsejoPintura;