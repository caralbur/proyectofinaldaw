import React, { Component } from 'react';
import 'antd/dist/antd.css';
import presentacion from '../img/presentacion.jpg';
import areaTrabajo from '../img/areaTrabajo.jpg';
import historia from '../img/historia.jpg';
import compromiso from '../img/compromiso.jpg';
import mision from '../img/mision.JPG';
import { Layout } from 'antd';

class Nosotros extends Component{
    render(){
        return(
        <Layout>
            <div className="row">
                    <header className="hdNosotros col-md-12 col-12">
                    <h1 className="tituloNosotros">NOSOTROS</h1>
                    </header>
                </div>

                <div className="contNosotros">

                    <div className="row" id="nos1">
                    <section id="contenido" className="col-md-12 col-12">
                        <div className="row">
                            <img className="col-md-6 col-12"  id="imgPresentacion" src={presentacion} alt="Error en imagen"/>
                            <div className="textpres col-md-6">
                                <h2 className="col-md-12 col-12"> Presentación </h2>
                                <p className="col-md-12 col-12"> SERVITEC realiza trabajos en todo Ecuador, contando con un buen servicio en lo que concierne realizar una cotización de los elementos de buena calidad necesarios  y la mano de obra para llevar a cabo el proyecto. </p>
                            </div>
                        </div >
                    </section>
                    </div>

                    <div className="row" id="nos2">
                    <section id="areaTrabajo" className="col-md-12 col-12" >
                        <div className="row" >
                            <img className="col-md-6 col-12 " style={{ height: '500px' }} src={areaTrabajo} alt="Error en imagen"/>
                            <div className="texttrab col-md-6 col-12">
                                <h2 className="col-md-12 col-12"> Área de Trabajo </h2>
                                <p className="col-md-12 col-12"> SERVITEC proporciona servicios tanto de construcción, reparaciones, remodelaciones y electricidad a nivel nacional. </p>
                            </div>
                        </div >
                    </section>
                    </div>

                    <div className="row" id="nos3">
                    <section id="historia" class="col-md-12 col-12">
                        <div className="row">
                        <img className="col-md-6 col-12" style={{ height: '400px' }} src={historia} alt="Error en imagen"/>
                        <div className="texthist col-md-6 col-12">
                            <h2 className="col-md-12 col-12"> Historia</h2>
                            <p className="col-md-12 col-12"> SERVITEC es una empresa que desde hace 37 años se dedica a ofrecer servicios de construcción y electricidad de calidad permitiendo que nuestros clientes obtengan un servicio de excelencia. </p>
                        </div>
                        </div >
                    </section>
                    </div>


                    <div className="row" id="nos4">
                    <section id="compromiso" class="col-md-12 col-12">
                        <div className="row">
                        <img className="col-md-6 col-12" style={{ height: '400px' }} src={compromiso} alt="Error en imagen"/>
                        <div className="textcomp col-md-6 col-12">
                            <h2 className="col-md-12 col-12"> Compromiso</h2>
                            <p className="col-md-12 col-12"> SERVITEC se compromete con sus clientes a proporcionar un producto final impecable, el mismo que será supervisado día a día en el tiempo de construcción. </p>
                        </div>
                        </div >
                    </section>
                    </div>

                    <div className="row" id="nos5">
                    <section id="mision" class="col-md-12 col-12">
                        <div className="row">
                            <img className="col-md-6 col-12" style={{ height: '400px' }} src={mision} alt="Error en imagen"/>
                        <div className="textmisvis col-md-6 col-12">
                            <div className="row col-md-12 col-12">
                            <h2 className="col-md-12 col-12"> Misión </h2>
                            <p className="col-md-12 col-12"> Fortalecer la relación con nuestros clientes a través del servicio de calidad demostrado en cada trabajo realizado. </p>
                            </div>
                            <div className="row col-md-12 col-12">
                            <h2 className="col-md-12 col-12"> Visión </h2>
                            <p className="col-md-12 col-12"> Ser líder en referencias del mercado de la construcción y electricidad a base de la calidad e innovación brindada en cada uno de los proyectos. </p>
                            </div>
                        </div>
                        </div >
                    </section>
                    </div>

                </div>
        </Layout>
        )
    };
}

export default Nosotros;



