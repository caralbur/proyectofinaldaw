import React, { Component } from 'react';
import 'antd/dist/antd.css';
import ladrillococido from '../img/ladrillococido.jpg';
import ladrilloperforado from '../img/ladrilloperforado.jpg';
import ladrilloperforadocanto from '../img/ladrilloperforadocanto.jpg';
import ladrilloadobe from '../img/ladrilloadobe.jpg';
import ladrillocemento from '../img/ladrillocemento.jpg';
import { Layout } from 'antd';
import { Carousel } from 'antd';

class Consejos extends Component{
    render(){
        return(
        <Layout>
            <div id='contenidoCons'>
                    <div className="row">
                        <header class="col-xl-12">
                        <h1 id="hdConsejos" className="col-xl-12">Consejos</h1>
                        </header>
                    </div>

                    <div className="row">
                        <section id="secConsejos" className="col-md-12 col-12">
                        <h3 id="tl0" className="col-xl-12">.</h3>
                        <div className="row">
                            <article className="col-md-12">
                            <h3 > Conoce los tipos de ladrillos y sus principales usos </h3>
                            <h4 >LADRILLO COCIDO DE ARCILLA</h4>
                            <img className="imagenCons col-md-12 col-12" style={{ height: '60%', width: '30%' }} src={ladrillococido} alt="Imagen not found"/>
                            <p className="descCons"> Es el más común de todos. Su uso está muy diversificado en el ramo de la construcción. Se elabora de forma totalmente artesanal y carece de agujeros.</p>
                            </article>
                        </div>

                        <div>
                            <article>
                            <h4 > LADRILLO DE TIERRA PERFORADO</h4>
                            <img className="imagenCons col-md-12 col-12" style={{ height: '60%', width: '30%' }} src={ladrilloperforado} alt="Imagen not found"/>
                            <p className="descCons"> Si lo que deseas es hacer aún más resistente tu muro, este ladrillo es el indicado. Los agujeros en su superficie permiten el ingreso del mortero o concreto fortaleciendo la construcción.</p>
                            </article>
                        </div>
                        <div>
                            <article>
                            <h4> LADRILLO PERFORADO AL CANTO O HUECO </h4>
                            <img className="imagenCons col-md-12 col-12" style={{ height: '60%', width: '30%' }} src={ladrilloperforadocanto} alt="Imagen not found"/>
                            <p className="descCons"> Estos ladrillos tienen perforaciones horizontales. Sus huecos brindan ligereza, por lo que reducen la carga. Los hay de formato tradicional o gran formato, según las necesidades del constructor.</p>
                            </article>
                        </div>
                        <div>
                            <article>
                            <h4> ADOBE </h4>
                            <img className="imagenCons col-md-12 col-12" style={{ height: '60%', width: '30%' }} src={ladrilloadobe} alt="Imagen not found"/>
                            <p className="descCons"> Este material, es usado en construcciones de tipo rústico o de un nivel. A diferencia del ladrillo cocido de arcilla, éste se seca completamente al sol, por lo que sus cualidades térmicas son mayores. Sin embargo, por su poca resistencia, no es recomendable para construcción de viviendas.</p>
                            </article>
                        </div>
                        <div>
                            <article>
                            <h4> BLOQUE DE CEMENTO </h4>
                            <img className="imagenCons col-md-12 col-12" style={{ height: '60%', width: '30%' }} src={ladrillocemento} alt="Imagen not found"/>
                            <p className="descCons"> El bloque de cemento es un material prefabricado que se utiliza principalmente para construir muros. Al igual que los ladrillos comunes, los bloques funcionan en conjunto al apilarse y al unirse con mortero formado generalmente por cemento, arena y agua. Para llevar a cabo esta unión, los bloques presentan un interior hueco que permite el paso de las barras de acero y el relleno de mortero.</p>
                            </article>
                        </div>
                        </section>
                    </div>

                    
                    <Carousel autoplay>
                        <div><h3><a href={"/consejoconstruccion/"}>¿Como construir tu propia casa?</a></h3></div>
                        <div><h3><a href={"/consejopintura/"}>Tipos de pinturas y uso</a></h3></div>
                        <div><h3><a href={"/consejodiseno/"}>¿Como se si mi casa esta bien diseñada?</a></h3></div>
                    </Carousel>
                       

            
            </div>
        </Layout>
        )
    };

}

export default Consejos;







