import React, { Component } from 'react';
import 'antd/dist/antd.css';
import quienes from '../img/quienes.jpg';
import acerca from '../img/acerca.jpg';
import contactenos from '../img/contactenos.jpg';
import { Layout } from 'antd';


class Presentation extends Component{
    render(){
        return (
        <Layout>
            <div className="col-md-12 col-12">
                <header id="hdInicio" className="col-md-12 col-12">
                <div id="hdFondo" className="col-md-12 col-12"> </div>
                    <h1 className="col-md-12 col-12"> SERVITEC </h1>
                    <h2 className="col-md-12 col-12"> Servicio de construcción y electricidad con años de experiencia en sectores públicos y privados </h2>
                </header>
                </div>
                <div className="row">
                    <section id="secInicio" className="col-xl-12 col-12">
    
                        <h3 id="tl0" className="col-xl-12 d-none d-md-block"></h3>
    
                        <div className="row">
                            <article className="col-md-4 col-12">
                                <div className="row">
                                    <h3 className="col-md-12 col-12"> ¿Quiénes somos? </h3>
                                    <figure className="col-md-3 d-none d-md-block">
                                    <img src={quienes} alt="Error en imagen"/>
                                    </figure>
                                    <p className="col-md-9 col-12"> SERVITEC proporciona servicios tanto de construcción, electricidad, y más a nivel nacional. <a id="masCr" class="nav-link" href="">Leer más.</a> </p>
                                </div>
                                </article>
    
                                <article className="col-md-4 col-12">
                                    <div className="row">
                                        <h3 className="col-md-12 col-12"> Acerca De </h3>
                                        <figure className="col-md-3 d-none d-md-block">
                                            <img src={acerca} alt="Error en imagen"/>
                                        </figure>
                                        <p className="col-md-9 col-12"> SERVITEC realiza trabajos en todo Ecuador, contando con un buen servicio tanto de mano de obra como en proporcionar los productos de mejor calidad. <a id="masN" class="nav-link" href="">Leer más.</a> </p>
                                    </div>
                                </article>
    
                                <article className="col-md-4 col-12">
                                    <div className="row">
                                        <h3 className="col-md-12 col-12"> Contáctenos </h3>
                                        <figure className="col-md-3 d-none d-md-block">
                                        <img src={contactenos} alt="Error en imagen"/>
                                        </figure>
                                        <p className="col-md-9 col-12"> Nos pueden contactar para cualquier servicio de construcción y electricidad tanto vía e-mail como telefónica. <a id="masCn" class="nav-link" href="">Leer más.</a> </p>
                                      </div>
                                </article>
                        </div>
                    </section>  
            </div>
            </Layout>
        );
    }
    
}

export default Presentation;