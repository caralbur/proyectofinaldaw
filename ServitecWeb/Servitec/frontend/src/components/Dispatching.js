import React, { Component }  from 'react';
import 'antd/dist/antd.css';
import {
    select,
    scaleBand,
    scaleLinear,
}from 'd3';
import { updateLocale } from 'moment';



class Dispatching extends React.Component {
    constructor(){
        super();
        this.state = {
            varFinal: [],
            data: [], 
            dataTemp:[],
            sinRepetir:[],
            valoresRepetidos:[] 
            
        }
    }


    componentWillMount() {
        fetch("https://data.cityofnewyork.us/resource/b2iz-pps8.json")
        .then((response) => {
            return response.json()
          })

          .then((linea) => {
            this.setState({ 
                data:linea
            })    
        })
    }
   
    
    draw(){
        const node=select(this.node);
        const bounds= node.node().getBoundingClientRect();
        const w=bounds.width;
        const h= bounds.height;
        const { data }= this.state;

        
        const xscale=scaleBand();    
        xscale.domain(this.state.varFinal.map(d=>d.ciudad));
        xscale.range([0,w]);
        xscale.padding(0.1);

        const yscale=scaleLinear();
        yscale.domain([0,100]);
        yscale.range([0,h]);

        const upd=node.selectAll('rect').data(this.state.varFinal);
        upd.enter()
            .append('rect')
            .attr('x',d=>xscale(d.ciudad))
            .attr('y',d=>h-yscale(d.numero))
            .attr('width',xscale.bandwidth())
            .attr('height',d=> yscale(d.numero))
            .attr('fill','skyblue');
    }

    componentDidUpdate(){
        this.draw();
    }
        
    
    componentDidMount() {
        this.draw();
    }

   // el tamaño del espacio en blanco interior 'donut'
    
    render(){

        var paso;

        for (paso = 0; paso < this.state.data.length; paso++) {
            this.state.dataTemp[paso] = this.state.data[paso].boro
        }

        Array.prototype.unique = function(a){
            return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
        });
        this.state.sinRepetir = this.state.dataTemp.unique()

        var i;
        var j;
        for(i = 0; i < this.state.sinRepetir.length; i++){
            var k = 0;
            for(j = 0; j < this.state.dataTemp.length; j++){
                if(this.state.sinRepetir[i] == this.state.dataTemp[j]){
                    k = k + 1;
                    this.state.valoresRepetidos[i] = k;
                }
            }
        }

        var l;
        for(l = 0; l < this.state.sinRepetir.length; l++){
            this.state.varFinal[l]= {'ciudad':this.state.sinRepetir[l],'numero':this.state.valoresRepetidos[l]};
        }

        
        return(
            <div className="row">
                <div className="col-md-6 col-6">
                    <table border="2">
                        <thead>
                            <tr>
                                <th>Country</th>
                                <th>HistoricalDataSymbol</th> 
                            </tr>
                        </thead>
                        <tbody>  
                           {this.state.varFinal.map(art => {
                            return (
                                <tr>
                                    <td>{art.ciudad}</td>
                                    <td>{art.numero}</td>
                                </tr>
                            );
                            })}
                        </tbody>
                    </table>
                </div>
                <div className="col-md-6 col-6">
                    <div className="col-md-12 col-12">
                            <h3> Grafica #DeMultas vs Ciudad </h3>
                    </div>
                    <div className="col-md-12 col-12">
                        <svg
                        style={{width:'80%', height:'200%'}}
                        ref={node=>{
                            this.node=node;
                        }}                    
                        >
                        </svg>
                    </div>
                    
                </div>
                
            </div> 
            
        );
        
    }


}

export default Dispatching;
