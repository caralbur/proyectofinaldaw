import React, { Component } from 'react';
import 'antd/dist/antd.css';
import arquitectonico from '../img/DisenoArquitectonico.jpg';
import estructural from '../img/disenoEstructural.jpg';
import electrico from '../img/disenoElectrico.jpg';
import sanitario from '../img/disenoSanitario.jpg';
import { Layout } from 'antd';
import { Carousel } from 'antd';

class ConsejoDiseno extends Component{
    render(){
        return(
        <Layout>
            <div id='contenidoCons'>
                    <div className="row">
                        <header className="col-xl-12">
                        <h1 id="hdConsejos" className="col-xl-12">Consejos</h1>
                        </header>
                    </div>

                    <div className="row">
                        <section id="secConsejos" className="col-md-12 col-12">
                        <h3 id="tl0" className="col-xl-12">.</h3>
                        <div className="row">
                            <article className="col-md-12">
                            <h3 > Como saber si tu casa esta bien diseñada </h3>
                            <h4 > DISEÑO ARQUITECTÓNICO </h4>
                            <img className="imagenCons col-md-12 col-12" style={{ height: '60%', width: '30%' }} src={arquitectonico} alt="Imagen not found"/>
                            <p className="descCons"> Aquí lo que se define es la distribución de los diferentes ambientes de la vivienda, se cuida que estén bien orientados, ventilados e iluminados con luz natural, también que haya el espacio suficiente en cada uno de ellos y además se define la estética de la vivienda. </p>
                            </article>
                        </div>

                        <div>
                            <article>
                            <h4 > DISEÑO ESTRUCTURAL </h4>
                            <img className="imagenCons col-md-12 col-12" style={{ height: '60%', width: '30%' }} src={estructural} alt="Imagen not found"/>
                            <p className="descCons"> En este tipo de diseño se define minuciosa y cuidadosamente las características técnicas de cada uno de los elementos (vigas, columnas, etc.) que conforman la estructura de una vivienda de tal manera que esta tenga la fortaleza necesaria y suficiente para que la vivienda sea segura y así pueda soportar exitosamente su enorme peso y los efectos dañinos de los sismos. </p>
                            </article>
                        </div>
                        <div>
                            <article>
                            <h4> DISEÑO ELECTRICO </h4>
                            <img className="imagenCons col-md-12 col-12" style={{ height: '60%', width: '30%' }} src={electrico} alt="Imagen not found"/>
                            <p className="descCons"> Determina las características técnicas de cada uno de los diferentes circuitos eléctricos que se utilizan en una vivienda. </p>
                            </article>
                        </div>
                        <div>
                            <article>
                            <h4> DISEÑO SANITARIO </h4>
                            <img className="imagenCons col-md-12 col-12" style={{ height: '60%', width: '30%' }} src={sanitario} alt="Imagen not found"/>
                            <p className="descCons"> Define las características técnicas de las redes de agua fría y caliente, de modo que estas preserven la potabilidad, garantizando un suministro sin ruidos, en cantidades y presiones suficientes en los caños, duchas, etc. por otro lado, definen también las características de las redes de desagüe y ventilación. </p>
                            </article>
                        </div>
                        </section>
                    </div>

                    
                    <Carousel autoplay>
                        <div><h3><a href={"/consejoconstruccion/"}>¿Como construir tu propia casa?</a></h3></div>
                        <div><h3><a href={"/consejos/"}>Conoce los tipos de ladrillos y sus principales usos</a></h3></div>
                        <div><h3><a href={"/consejopintura/"}>Tipos de pinturas y uso</a></h3></div>
                    </Carousel>
                       

            
            </div>
        </Layout>
        )
    };

}

export default ConsejoDiseno;