import React, { Component } from 'react';
import 'antd/dist/antd.css';
import tr2 from '../img/tr2.jpg';
import tr from '../img/tr.jpg';
import tr3 from '../img/tr3.jpg';
import tr4 from '../img/tr4.jpg';
import tr5 from '../img/tr5.jpg';
import tr6 from '../img/tr6.jpg';
import { Layout } from 'antd';
import { Timeline, Icon } from 'antd';

class Trabajos extends Component{
    render(){
        return(
        <Layout>
            <div className="row">
                <header className="col-md-12 col-12">
                    <h1 id="tlTra" className="col-md-12 col-12">Trabajos Previos</h1>
                </header>
            </div>

            <div className="row col-md-12">
                <div className="row col-md-8 col-12">
                    <section className="secTrab col-12">
                        <h3 id="tl0" className="col-md-12 col-12">.</h3>
                        <div className="row"  id="im1">
                            <img id="imTr1" className="col-md-12 d-none d-md-block" style={{ height: '50%' }} src={tr2} alt="Image Not Found"/>
                        </div>

                        <div className="row" id="im2">
                            <img id="imTr2" className="col-md-6 col-12" style={{ height: '50%' }} src={tr} alt="Image Not Found"/>
                            <img id="imTr3" className="col-md-6 col-12" style={{ height: '50%' }} src={tr3} alt="Image Not Found"/>
                        </div>

                        <div class="row"  id="im3">
                            <img id="imTr4" className="col-md-6 col-12" style={{ height: '50%' }} src={tr4} alt="Image Not Found"/>
                            <img id="imTr5" className="col-md-6 col-12" style={{ height: '50%' }} src={tr5} alt="Image Not Found"/>
                        </div>

                        <div class="row"  id="im4">
                            <img id="imTr6" className="col-md-12 d-none d-md-block" style={{ height: '50%' }} src={tr6} alt="Image Not Found"/>
                        </div>
                    </section>
                </div>

                <aside id="timeline" className="col-md-4 d-none d-md-block">
                    <Timeline mode="alternate">
                        <Timeline.Item  className="h3"> <h4> 1981 - La empresa servitec comienza en el año 1981 gracias al señor Carlos Burgos. </h4></Timeline.Item>
                        <Timeline.Item  className="h3" color="green"><h4> 2016 - La empresa SERVITEC se encargó de la remodelación de las aulas en el edificio CELEX destinadas para impartir clases con el método Peer Project Learning. </h4></Timeline.Item>
                        <Timeline.Item  className="h3" color="red"><h4> 2017 - La empresa SERVITEC trabaja en la remodelación y regeneración de haciendas ubicadas en San Juan desde el año 2017 hasta la actualidad. </h4></Timeline.Item>   
                        <Timeline.Item  className="h3" dot={<Icon type="clock-circle-o"/>}><h5>2018 - Servitec acepta el cambio tecnológico y promueve la creación de su página web en beneficio de sus clientes.</h5></Timeline.Item>
                    </Timeline>
                </aside>
            </div>
        </Layout>
        )
    }
}

export default Trabajos;





