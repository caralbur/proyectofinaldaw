import React, { Component } from 'react';
import 'antd/dist/antd.css';
import ubicacion from '../img/casaUbicacion.jpg';
import arquitecto from '../img/casaPlano.jpg';
import mano from '../img/casaAlbaniles.jpg';
import diseno from '../img/casaNecesidades.jpeg';
import espacio from '../img/casaAmbiente.jpg';
import { Layout } from 'antd';
import { Carousel } from 'antd';

class ConsejoConstruccion extends Component{
    render(){
        return(
        <Layout>
            <div id='contenidoCons'>
                    <div className="row">
                        <header class="col-xl-12">
                        <h1 id="hdConsejos" className="col-xl-12">Consejos</h1>
                        </header>
                    </div>

                    <div className="row">
                        <section id="secConsejos" className="col-md-12 col-12">
                        <h3 id="tl0" className="col-xl-12">.</h3>
                        <div className="row">
                            <article className="col-md-12">
                            <h3 > Consejos para construir tu casa </h3>
                            <h4 > Ubicación y condiciones del terreno </h4>
                            <img className="imagenCons col-md-12 col-12" style={{ height: '60%', width: '30%' }} src={ubicacion} alt="Imagen not found"/>
                            <p className="descCons"> Una de las primeras cosas a considerar para construir una casa es, la ubicación o el terreno. Dicen por ahí que, el orden de los factores no altera el producto, es decir, es lo mismo tener primero el arquitecto o el lugar. Sin embargo, todo es posible. Saber primero de la ubicación o el terreno donde se desea construir, se tendrá un mejor idea del diseño de la vivienda. </p>
                            </article>
                        </div>

                        <div>
                            <article>
                            <h4 > Arquitecto y planos que se adecuen a tus necesidades </h4>
                            <img className="imagenCons col-md-12 col-12" style={{ height: '60%', width: '30%' }} src={arquitecto} alt="Imagen not found"/>
                            <p className="descCons"> Muchas veces los resultados no son los que se tenían planeado y es mejor consultar a un arquitecto. El arquitecto ayudará a determinar medidas, espacios, completar las ideas y más cosas de importancia, como electricidad, corriente de agua, detalles que no sabrás como manejarlos. </p>
                            </article>
                        </div>
                        <div>
                            <article>
                            <h4> Mano de obra – Buscar la mejor y de acuerdo a tu presupuesto </h4>
                            <img className="imagenCons col-md-12 col-12" style={{ height: '60%', width: '30%' }} src={mano} alt="Imagen not found"/>
                            <p className="descCons"> Para construir una casa es necesario llamar a los expertos. Tú mismo no te puedes echar andar y pegar ladrillo y tabique, tal vez sí, pero siempre es mejor que alguien que sepa lo haga. Los albañiles, como les decimos aquí en Ecuador, son lo más importante para la constricción de la casa. Ellos saben la medida perfecta del agua, cal, cemento, arena, y demás para poder formar los muros, la base. Así como la colocación de varillas para la cimentación. </p>
                            </article>
                        </div>
                        <div>
                            <article>
                            <h4> Diseño funcional y a tu gusto </h4>
                            <img className="imagenCons col-md-12 col-12" style={{ height: '60%', width: '30%' }} src={diseno} alt="Imagen not found"/>
                            <p className="descCons"> Después de tener los tres pasos anteriores, ahora es tiempo de pensar en el diseño. Es decir, si va a ser de dos pisos, cuantas habitaciones, que tendrá el primer. Por lo regular si es de dos pisos es para dos personas que están en planes futuros a formar una familia. En el primer piso, es recomendable la cochera, el recibidor, sala, comedor, cocina y por qué no, un pequeño patio. Por más sencilla, no puede faltar un medio baño. En la planta de arriba se puede añadir, recamaras, baño o baños completos, incluso, un estudio. Todo es cuestión a tus necesidades. </p>
                            </article>
                        </div>
                        <div>
                            <article>
                            <h4> Espacio para vivir con calidad </h4>
                            <img className="imagenCons col-md-12 col-12" style={{ height: '60%', width: '30%' }} src={espacio} alt="Imagen not found"/>
                            <p className="descCons"> Los diferentes ambientes de nuestra casa pueden encontrarse claramente separados o unidos en un espacio mayor. Lo que importa es que la espacialidad nos inspire, que no genere sensación de ahogo. Observemos cómo en este proyecto se ha estudiado la incidencia del sol en el espacio. ¡Qué grato que resulta encontrarse en un ambiente como éste! </p>
                            </article>
                        </div>
                        </section>
                    </div>

                    
                    <Carousel autoplay>
                        <div><h3><a href={"/consejos/"}>Conoce los tipos de ladrillos y sus principales usos</a></h3></div>
                        <div><h3><a href={"/consejopintura/"}>Tipos de pinturas y uso</a></h3></div>
                        <div><h3><a href={"/consejodiseno/"}>¿Como se si mi casa esta bien diseñada?</a></h3></div>
                    </Carousel>
                       

            
            </div>
        </Layout>
        )
    };

}

export default ConsejoConstruccion;
