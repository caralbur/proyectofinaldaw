import React, { Component } from 'react';
import 'antd/dist/antd.css';
import MapWithAMarker from '../containers/MapServitec';
import { Layout } from 'antd';

class Contactenos extends Component{
    render(){
        return(
            <div className="row">
                <header className="hdContac col-md-12">
                <h1 id="hdFormulario"> FORMULARIO DE CONTACTO </h1>
                </header>
                <section id="secCont" className="col-md-12">
                <div id="infoServitec" className="row">
                    <MapWithAMarker>
                    containerElement={<div className="ant-col-xs-24 ant-col-sm-24 ant-col-md-6" 
                    style={{ height: '25px' }}/>}
                    mapElement={<div className="ant-col-xs-24 ant-col-sm-24 ant-col-md-6" 
                    style={{ height: '25px' }}/>}
                    </MapWithAMarker>
                    <div className="infoEmpresa col-md-12 col-12 ">
                        <h2 className="col-md-12 col-12">* S E R V I T E C *</h2>
                        <p className="col-md-12 col-12">Telefono de la Empresa: (+593-994123771)</p>
                        <p className="col-md-12 col-12">Correo de la Empresa: carlosburgos63@hotmail.com</p>
                        <p className="col-md-12 col-12">Dirección de la Empresa: Lot. Mucho Lote Mz.3030 Sl.123 (3er Callejon, 8va Peatonal)</p>
                        <p className="col-md-12 col-12">Guayaquil - Ecuador</p>
        
                        <div className="formulario">
                        <form id="contactoFormulario" className="contacto" name="formulario" action="mailto:carlosburgos63@hotmail.com" method="post" enctype="text/plain">
                            <h3>¿Deseas que te contactemos?</h3>
                            <div>
                                <label className="col-md-12 col-12" for="nombre"> Nombre </label>
                                <input className="formularioInput form-text text-muted col-md-6 col-12 " type="text" id="nombre" name="Nombre" size="30" maxlength="50" placeholder="Ingrese su nombre" required/>
                            </div>
                            <div>
                                <label className="col-md-12 col-12" for="correoElectronico"> Correo Electronico </label>
                                <input className="formularioInput form-text text-muted col-md-6 col-12" type="text" id="correoElectronico" name="E-mail" placeholder="Ingrese su Correo" pattern="[a-z0-9._-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required/>
                            </div>
                            <div>
                            <label className="col-md-12 col-12" for="ciudad"> Ciudad </label>
                            <input className="formularioInput form-text text-muted col-md-6 col-12" type="text" id="ciudad" name="Ciudad" maxlength="50" placeholder="Ingrese su ciudad" required/>
                            </div>
                            <div>
                            <label className="col-md-12 col-12" for="direccion"> Dirección </label>
                            <input className="formularioInput form-text text-muted col-md-6 col-12" type="text" id="direccion" name="Direccion" maxlength="100" placeholder="Ingrese su dirección" required/>
                            </div>
        
                            <div>
                            <label className="col-md-12 col-12" > Año de Nacimiento </label>
                            <input id="nacFormulario" class="formularioInput form-text text-muted col-md-6 col-12" list="añosultimos" name="Año de Nacimiento" placeholder="Escoja el año" pattern="[0-9]{4}" required/>
                            <datalist className="col-md-4 col-12" id="añosultimos">
                                <option value="1960"/>
                                <option value="1961"/>
                                <option value="1962"/>
                                <option value="1963"/>
                                <option value="1964"/>
                                <option value="1965"/>
                                <option value="1966"/>
                                <option value="1967"/>
                                <option value="1968"/>
                                <option value="1969"/>
                                <option value="1970"/>
                                <option value="1971"/>
                                <option value="1972"/>
                                <option value="1973"/>
                                <option value="1974"/>
                                <option value="1975"/>
                                <option value="1976"/>
                                <option value="1977"/>
                                <option value="1978"/>
                                <option value="1979"/>
                                <option value="1980"/>
                                <option value="1981"/>
                                <option value="1982"/>
                                <option value="1983"/>
                                <option value="1984"/>
                                <option value="1985"/>
                                <option value="1986"/>
                                <option value="1987"/>
                                <option value="1988"/>
                                <option value="1989"/>
                                <option value="1990"/>
                                <option value="1991"/>
                                <option value="1992"/>
                                <option value="1993"/>
                                <option value="1994"/>
                                <option value="1995"/>
                                <option value="1996"/>
                                <option value="1997"/>
                                <option value="1998"/>
                                <option value="1999"/>
                                <option value="2000"/>
                            </datalist>
                            </div>
                            <div>
                            <p className="col-md-12 col-12">Escribenos</p>
                            <textarea id="mensajeContactos" className="formularioInput col-md-6 col-12" name="Mensaje" rows="8" cols="80" > </textarea>
                            <div class="botones col-md-12 col-12">
                                <button id="botonFormulario" className="m-1 btn btn-primary col-md-2 col-5" type="submit">Enviar</button>
                                <button className="m-1 btn btn-primary col-md-2 col-5" type="reset">Limpiar</button>
                            </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
                </section>
            </div>          
        )

    };

}

export default Contactenos;




