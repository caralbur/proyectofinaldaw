import React, { Component } from 'react';
import 'antd/dist/antd.css';
import Moya from '../img/Moya.jpeg';
import Burgos from '../img/Burgos.jpg';
import Lecaro from '../img/placeholder.jpg';
import Macas from '../img/placeholder.jpg';
import tr3 from '../img/tr3.jpg';
import tr4 from '../img/tr4.jpg';
import tr5 from '../img/tr5.jpg';
import tr6 from '../img/tr6.jpg';
import { Layout } from 'antd';

class Creadores extends Component{
    render(){
      return(
      <div>
        <div id="creadBody1" class="row">
          <header className="col-xl-12 col-12">
          <h1 id="tituloCread" className="col-xl-12 col-12">Creadores de la Página</h1>
          </header>
        </div>

        <div id="creadBody2" className="row">
          <section id="secCreadores" className="col-xl-12 col-12">
            <h3 id="tl0" className="col-xl-12">.</h3>
            <article id="creador1">
            <div id="crCompleto1" className="row">
              <h2 id="nombre1" className="col-md-12 col-12">Angel Moya</h2>
              <div id="info1" className="col-md-6 col-9">
                <p className="col-md-12">Estudiante de la  carrera Ingeniería en Computación en la ESPOL (Escuela Superior Politécnica del Litoral).</p>
                <p className="col-md-12">Lenguajes: Java, Python, C++</p>
                <p id="ce1" className="col-md-12">Correo electrónico: lamoya@fiec.espol.edu.ec</p>
                <p className="col-md-12">Teléfono: 0990851889</p>
              </div>
              <figure className="col-md-6 col-4">
                <img id="imagen1"  style={{ height: '80%', width: '30%' }} src={Moya} alt="Image Not Found"/>
              </figure>
            </div>
          </article>

          <div className="row">
            <p id="linea1" className="col-md-12 d-none d-md-block">----------------------------------------------------------------------------------------------------------------------------</p>
          </div>

          <article id="creador2">
            <div id="crCompleto2" className="row">
              <h2 id="nombre2" className="col-md-12 col-12">Carolina Burgos</h2>
              <div id="info2" className="col-md-6 col-9">
                <p className="col-md-12">Estudiante de la  carrera Ingeniería en Computación en la ESPOL (Escuela Superior Politécnica del Litoral).</p>
                <p className="col-md-12">Lenguajes: Java, Python, C++</p>
                <p id="ce2" className="col-md-12">Correo electrónico: caralbur@fiec.espol.edu.ec</p>
                <p className="col-md-12">Teléfono: 0990348816</p>
              </div>
              <figure className="col-md-6 col-4">
                <img id="imagen2"  style={{ height: '80%', width: '30%' }} src={Burgos} alt="Image Not Found"/>
              </figure>
            </div>
          </article>

          <div className="row">
            <p id="linea2" className="col-md-12 d-none d-md-block">----------------------------------------------------------------------------------------------------------------------------</p>
          </div>

          <article id="creador3">
            <div id="crCompleto3" className="row">
              <h2 id="nombre3" className="col-md-12 col-12">Manuel Lecaro</h2>
              <div id="info3" className="col-md-6 col-9">
                <p className="col-md-12">Estudiante de la  carrera Ingeniería en Computación en la ESPOL (Escuela Superior Politécnica del Litoral).</p>
                <p className="col-md-12">Lenguajes: Java, Python, C++</p>
                <p id="ce3" className="col-md-12">Correo electrónico: mnlecaro@fiec.espol.edu.ec</p>
                <p className="col-md-12">Teléfono: 0962833891</p>
              </div>
              <figure className="col-md-6 col-4">
                <img id="imagen3"  style={{ height: '80%', width: '30%' }} src={Lecaro} alt="Image Not Found"/>
              </figure>
            </div>
          </article>

          <div className="row">
            <p id="linea3" className="col-md-12 d-none d-md-block">----------------------------------------------------------------------------------------------------------------------------</p>
          </div>

          <article id="creador4">
            <div id="crCompleto4" className="row">
              <h2 id="nombre4" className="col-md-12 col-12">Alex Macas</h2>
              <div id="info4" className="col-md-6 col-9">
                <p className="col-md-12">Estudiante de la  carrera Ingeniería en Computación en la ESPOL (Escuela Superior Politécnica del Litoral).</p>
                <p className="col-md-12">Lenguajes: Java, Python, C++</p>
                <p id="ce4" className="col-md-12">Correo electrónico: admacas@fiec.espol.edu.ec</p>
                <p className="col-md-12">Teléfono: 0968197691</p>
              </div>
              <figure className="col-md-6 col-4">
                <img id="imagen4"  style={{ height: '80%', width: '30%' }} src={Macas} alt="Image Not Found"/>
              </figure>
            </div>
          </article>

        </section>
        </div>
      </div>
      )
    };

}

export default Creadores;

