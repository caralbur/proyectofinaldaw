import React, { Component } from 'react';
import 'antd/dist/antd.css';
import Moya from '../img/Moya.jpeg';
import Burgos from '../img/Burgos.jpg';
import Lecaro from '../img/placeholder.jpg';
import Macas from '../img/placeholder.jpg';
import tr3 from '../img/tr3.jpg';
import tr4 from '../img/tr4.jpg';
import tr5 from '../img/tr5.jpg';
import tr6 from '../img/tr6.jpg';
import { Layout } from 'antd';
import BarChart from './chart';
import Dispatching from './Dispatching';
import BarChart3 from './chart3';

class Estadisticas extends Component{

    constructor(props) {
        super(props);
        
    }

    render(){
        return (
            <div className="row">
                <div id="Grafico1" className="col-md-12 col-12">
                    <div className="col-md-12 col-12" >
                        <h1>EcuadorGDP Para construcciones (The Gross Domestic Product)	</h1>
                    </div>

                    <div className="col-md-12 col-12" >
                        <p>El PIB de la construcción en Ecuador aumentó a 1557646 USD Miles en el tercer trimestre de 2018 desde 1557229 USD Miles en el segundo trimestre de 2018. El PIB de la construcción en Ecuador promedió 1196351.44 USD Miles desde 2000 hasta 2018, alcanzando un máximo histórico de 1775774 USD Miles en el tercer trimestre de 2014 y un mínimo histórico de 499868 USD Miles en el primer trimestre de 2000. </p>
                        <BarChart />
                        <p >Informacion obtenida de <a href="https://api.tradingeconomics.com/historical/country/Ecuador/indicator/GDP%20From%20Construction?client=guest:guest">www.tradingeconomics.com</a></p> 
                    </div>
                </div>

                <div id="Grafico2" className="col-md-12 col-12">
                    <div className="col-md-12 col-12" >
                            <h1>Cantidad de multas generadas por Vienes Raices con violaciones de construcción.	</h1>
                    </div>
                    <div className="col-md-12 col-12" >
                        <p>De conformidad con el Código de Mantenimiento de Viviendas de la Ciudad de Nueva York, el Departamento de Preservación y Desarrollo de Viviendas (HPD, por sus siglas en inglés) emite infracciones contra las condiciones en las unidades de vivienda de alquiler que han sido verificadas para violar la Ciudad de Nueva York utilizando el Código de Mantenimiento (HMC) o la Vivienda Múltiple del Estado de Nueva York Ley (MDL). Las violaciones se emiten cuando una inspección verifica que existe una violación de la HMC o MDL. Se cierra cuando se corrige la violación, según lo observado / verificado por HPD o según lo certificado por el propietario.</p>
                        <Dispatching />
                        <p>Informacion obtenida de <a href="https://data.cityofnewyork.us/Housing-Development/Housing-Maintenance-Code-Violations/wvxf-dwi5"> www.data.cityofnewyork.us</a></p>
                    </div>
                
                </div>

                <div id="Grafico3" className="col-md-12 col-12">
                    <div className="col-md-12 col-12" >
                            <h1>Cargos de orden de trabajo de Handyman (HWO)Desarrollo de vivienda</h1>
                    </div>
                    <div className="col-md-12 col-12" >
                        <p>Contiene información sobre las órdenes de trabajo creadas para realizar trabajos de reparación de emergencia cuando un propietario no aborda una condición peligrosa conforme a los requisitos de una infracción emitida por el HPD. </p>
                        <BarChart3 />
                        <p>Informacion obtenida de <a href="https://data.cityofnewyork.us/Housing-Development/Handyman-Work-Order-HWO-Charges/sbnd-xujn"> www.data.cityofnewyork.us</a></p>
                    </div>
                
                </div>

            </div>
            
      );
    };

}

export default Estadisticas;
