import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Layout } from 'antd';
import axios from 'axios';


class Profile extends Component{

	constructor(props) {
    super(props);
    this.state = {
			items: [],
			userna:"",
    };
	}
	
   componentDidMount() {
		this.setState({
			userna: localStorage.getItem("userna"),
		});
    fetch("http://127.0.0.1:8000/api-autent/user/")
      .then((response) => {
            return response.json()
       })
      .then(
        (result) => {
          this.setState({
						items: result,
          });
        }
			)
  }


	render(){
		return(

			<Layout>
				<div class="container">
					<div className="row">
				        <header className="col-md-12 col-12">
				            <h1>Perfil de Usuario</h1>
				        </header>
				    </div>
						<section>

							{this.state.items.map(function(item){
				 			if( localStorage.getItem("userna") === item.username){
				 			return (
							
							<div class='Contenido row'>
				    		<label class="col-md-6 col-6 text-right font-weight-bold">User:</label><label class="col-md-6 col-6 text-left">{item.username}</label>
				    		<label class="col-md-6 col-6 text-right font-weight-bold">Nombre:</label><label class="col-md-6 col-6 text-left">{item.first_name}</label>
				    		<label class="col-md-6 col-6 text-right font-weight-bold">Apellido:</label><label class="col-md-6 col-6 text-left">{item.last_name}</label>
				    		<label class="col-md-6 col-6 text-right font-weight-bold">Email:</label><label class="col-md-6 col-6 text-left">{item.email}</label>
							<label class="col-md-6 col-6 text-right font-weight-bold">Última Conexión:</label><label class="col-md-6 col-6 text-left">{item.last_login}</label>
				    		</div>
				 			);
				 		}
				 		})}  		
					
					</section>

				</div>
				</Layout>
			)
	}
		
}

export default Profile;
