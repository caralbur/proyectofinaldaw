import React, { Component } from 'react';
import { List, Avatar, Icon, Button } from 'antd';

const IconText = ({ type, text }) => (
    <span>
      <Icon type={type} style={{ marginRight: 8 }} />
      {text}
    </span>
);

const Cotizaciones = (props) => {

    return(
        <List
        itemLayout="vertical"
        size="large"
        pagination={{
          onChange: (page) => {
            console.log(page);
          },
          pageSize: 3,
        }}
        dataSource={props.data}
        footer={<div>
            <b>Servitec</b> Articulos Comunes
            </div>}
        renderItem={item => (
          <List.Item
            key={item.title}
            actions={[<IconText type="star-o" text="156" />, <IconText type="like-o" text="156" />, <IconText type="message" text="2" />]}
            extra={<img width={272} alt="logo" src="https://gw.alipayobjects.com/zos/rmsportal/mqaQswcyDLcXyDKnZfES.png" />}
          >
            <List.Item.Meta
              avatar={<Avatar src={item.avatar} />}
              id = {item.id}
        title={<a href={`/cotizacion/${item.id}`}>{item.nombre}</a>}
              description={<div><h5>Precio: </h5>{item.precio}</div>}
            />
            {item.content}
          </List.Item>
        )}
      />
    )

};



export default Cotizaciones;