import axios from 'axios';
const API_URL = 'http://localhost:8000';

export default class CotizacionService{

    constructor(){}


    getCotizacion() {
        const url = `${API_URL}/api/frontend/articulo/`;
        return axios.get(url).then(response => response.data);
    }  
    getCotizacionByURL(link){
        const url = `${API_URL}${link}`;
        return axios.get(url).then(response => response.data);
    }
    getCotizacion(pk) {
        const url = `${API_URL}/api/frontend/articulo/${pk}`;
        return axios.get(url).then(response => response.data);
    }
    deleteCotizacion(work){
        const url = `${API_URL}/api/frontend/articulo/${work.id}`;
        return axios.delete(url);
    }
    createCotizacion(work){
        const url = `${API_URL}/api/frontend/articulo/`;
        return axios.post(url,work);
    }
    updateCotizacion(work){
        const url = `${API_URL}/api/frontend/articulo/${work.id}`;
        return axios.put(url,work);
    }
}