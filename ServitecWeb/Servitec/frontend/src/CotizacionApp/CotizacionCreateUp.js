import CotizacionService from './CotizacionService';

const cotizacionService = new CotizacionService();

class CotizacionCreateUpdate extends Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
      }

      componentDidMount(){
        const { match: { params } } = this.props;
        if(params && params.pk)
        {
          cotizacionService.getTrabajos(params.pk).then((c)=>{
            this.refs.descripcion.value = c.descripcion;
            this.refs.valorUnitario.value = c.valorUnitario;
            this.refs.estaTerminado.value = c.estaTerminado;
          })
        }
      }

      handleCreate(){
        cotizacionService.createTrabajo(
          {
            "referencia":this.refs.descripcion.value,
            "valorUnitario":this.refs.valorUnitario.value,
            "estaTerminado":this.refs.estaTerminado.value
        }          
        ).then((result)=>{
          alert("Trabajo creado!");
        }).catch(()=>{
          alert('Error Please re-check your form');
        });
      }
      handleUpdate(pk){
        trabajoService.updateTrabajo(
          {
            "referencia":this.refs.descripcion.value,
            "valorUnitario":this.refs.valorUnitario.value,
            "estaTerminado":this.refs.estaTerminado.value
        }          
        ).then((result)=>{
          console.log(result);
          alert("Trabajo actualizado!");
        }).catch(()=>{
          alert('Error Please re-check your form')
        });
      }
      handleSubmit(event) {
        const { match: { params } } = this.props;

        if(params && params.pk){
          this.handleUpdate(params.pk);
        }
        else
        {
          this.handleCreate();
        }

        event.preventDefault();
      }

      render() {
        return (
          <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>
              Descripcion:</label>
              <input className="form-control" type="text" ref='descripcion' />

            <label>
              valor unitario:</label>
              <input className="form-control" type="text" ref='valorUnitario'/>

            <label>
              Estado:</label>
              <input className="form-control" type="text" ref='estadoTerminado' />

            <input className="btn btn-primary" type="submit" value="Submit" />
            </div>
          </form>
        );
      }  
}

export default TrabajoCreateUpdate;