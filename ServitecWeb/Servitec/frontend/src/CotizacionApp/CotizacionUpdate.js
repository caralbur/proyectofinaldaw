import React, { Component } from 'react';
import axios from 'axios';
import CustomForm from '../containers/CustomForm';
import {Card, Button} from 'antd';

class CotizacionUpdate extends React.Component{
    state = {
        cotizacion:{}
    }

    componentDidMount(){
        const id = this.props.match.params.id;
        axios.get(`http://127.0.0.1:8000/api/articulo/${id}`)
            .then(res =>{
                this.setState({
                    cotizacion:res.data
                })
            })
            console.log(this.state.cotizacion.nombre)
    }

    handleDelete = (event) =>{
        const id = this.props.match.params.id;
        axios.delete(`http://127.0.0.1:8000/api/articulo/${id}`)
        this.props.history.push('/');
        this.forceUpdate();
    }

    render(){
        return(
            <div>
            <Card title={this.state.cotizacion.nombre}>
                <p className="h3">{this.state.cotizacion.nombre}</p>
            </Card>
            <CustomForm
                requestType="put"
                articleID={this.props.match.params.id}
                btnText="Update"/>
                <form onSubmit={this.handleDelete}>
                    <Button type="danger" htmlType="submit">
                    Delete
                    </Button>
                </form>

            </div>

        )
    }

}

export default CotizacionUpdate;