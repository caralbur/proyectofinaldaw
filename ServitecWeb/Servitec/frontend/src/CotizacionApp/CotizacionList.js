import React, { Component } from 'react';
import Cotizaciones from './Cotizacion';
import axios from 'axios';
import CustomForm from '../containers/CustomForm';

const count = 3;
const fakeDataUrl = `http://127.0.0.1:8000/api/articulo/`;


class CotizacionList extends React.Component {
    state = {
        data: [],
        list: [],
    }
    
    componentDidMount() {
        axios.get('http://127.0.0.1:8000/api/articulo/')
        .then(res =>{
            this.setState({
                data: res.data,
                list:res.data
            });
        })
    }
  

    render(){
        return(
            <div>
            <Cotizaciones data={this.state.data}/>
            <br/>
            <h2>Ingresar un Insumo</h2>
            <CustomForm
                requestType="post"
                articleID={null}
                btnText="Create"/>
            </div>

        )
    }
}

export default CotizacionList;