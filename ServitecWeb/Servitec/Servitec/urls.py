"""Servitec URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from apps.vistas import views
from django.contrib.auth.views import LoginView 
from apps.frontend.api import urls
from django.views.decorators.csrf import csrf_exempt
from django.conf.urls.static import static
from django.conf import settings
admin.autodiscover()
urlpatterns = [
    path('admin/', admin.site.urls),
    #path('accounts/', include('django.contrib.auth.urls')),
    #url('',include('apps.vistas.urls',namespace='vistas')),
    url('usuario/',include('apps.usersDivision.urls')), 
    #url('',LoginView.as_view(template_name='login.html'),name='login_usuario'), 
    path('rest-auth/', (include('rest_auth.urls'))),
    #url(r'^login/',LoginView.as_view(template_name='login.html'),name='login_usuario'), 
    path('api-auth/', include('rest_framework.urls')),
    path('api/',include('apps.frontend.api.urls')),
    path('api-autent/', include('apps.usersDivision.api.urls')),
    path('rest-auth/registration',include('rest_auth.registration.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
